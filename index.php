<?php
$kanji       = $_GET['kanji'] ?? '';
$font_family = $_GET['font_family'] ?? 'Yu Gothic';

$kanji_unicode_hex = ltrim(unpack('H*', iconv('UTF-8', 'UCS-4BE', $kanji))[1],'0');
$kanji_shift_jis   = ltrim(unpack('H*', mb_convert_encoding($kanji, 'SJIS', 'UTF8'))[1],'0'); 
?>
<html>
<head>
<title>Kanji variants</title>
<style>
.variants { font-size: 36px; font-family: "<?= $font_family; ?>"}

.jis78 {font-variant-east-asian: jis78}
.jis83 {font-variant-east-asian: jis83}
.jis90 {font-variant-east-asian: jis90}
.jis04 {font-variant-east-asian: jis04}
</style>
</head>
<body>
<h1>Kanji variants</h1>
<form>
<label for="kanji_input">Kanji:</label>
<input id="kanji_input" name="kanji" value=<?= $kanji ?>></input>
<label for="font_family">Font family:</label>
<input id="font_family_input" name="font_family" list="font_family_list" value="<?= $font_family ?>"></input>
<datalist id="font_family_list">
<option>Meiryo</option>
<option>Meiryo UI</option>
<option>MS Gothic</option>
<option>MS Mincho</option>
<option>MS PGothic</option>
<option>MS PMincho</option>
<option>MS UI Gothic</option>
<option>Noto Sans CJK JP</option>
<option>Noto Serif CJK JP</option>
<option>Yu Gothic</option>
<option>Yu Gothic UI</option>
<option>Yu Mincho</option>
</datalist>
<input type="submit"/>
</form>
<hr/>
<?php if ('' !== $kanji): ?>
<h2>Standard variant</h2>
<p>Unicode: <?= $kanji_unicode_hex ?>; Shift-JIS: <?= $kanji_shift_jis ?></p>
<table>
<tr>
<td>Default</td>
<td>JIS78</td>
<td>JIS83</td>
<td>JIS90</td>
<td>JIS04</td>
</tr>
<tr>
<td class="variants"><?= $kanji ?></td>
<td class="variants jis78"><?= $kanji ?></td>
<td class="variants jis83"><?= $kanji ?></td>
<td class="variants jis90"><?= $kanji ?></td>
<td class="variants jis04"><?= $kanji ?></td>
</tr>
</table>
<h2>Standardized variation sequences (SVS)</h2>
<table>
<tr><?php for ($i=0; $i<16; $i++): ?><td class="variants"><?php echo $kanji . '&#' . (0xFE00 + $i) . ';' ?></td><?php endfor; ?></tr>
</table>
<h2>Ideographic variation sequences (IVS)</h2>
<table>
<tr><?php for ($i=0; $i<16; $i++): ?><td class="variants"><?php echo $kanji . '&#' . (0xE0100 + $i) . ';' ?></td><?php endfor; ?></tr>
</table>
<h2>Cross references</h2>
<ul>
<li><a href="http://glyphwiki.org/wiki/u<?= $kanji_unicode_hex ?>">Glyphwiki</a></li>
<li><a href="https://ja.wiktionary.org/wiki/<?= $kanji ?>">Wiktionary [jp]</a></li>
<li><a href="https://en.wiktionary.org/wiki/<?= $kanji ?>">Wiktionary [en]</a></li>
</ul>
<?php endif; ?>
<h2>References</h2>
<ul>
<li>Source: <a href="https://gitlab.com/Zalmoksis/kanji-variants">Kanji Variants</a> (GitLab)</li>
<li>Wikipedia: <a href="https://en.wikipedia.org/wiki/Variant_form_(Unicode)">Variant form (Unicode)</a></li>
</ul>
</body>
</html>
