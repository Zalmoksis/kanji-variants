# Kanji Variants

Source code for: http://kanji-variants.zalmoksis.pl

A simplistic tool to preview rendering of variants of chinese character (Jap. kanji). Currently displaying the selected character using:
 * CSS OpenType features rendering glyphs according to different versions of JIS standard
 * the Unicode variation sequence mechanisms: SVS and IVS.

References:
 * https://en.wikipedia.org/wiki/Variant_form_(Unicode)

